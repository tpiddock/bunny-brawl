﻿using UnityEngine;

public class AnimationController : MonoBehaviour
{
    
    //helpers 
    public GameObject Bunny;
    private Animator anim;
    private PlayerController.StateEnum currentState;
    private PlayerController.StateEnum prevState;
    
    void Start()
    {
        anim = (Animator)Bunny.GetComponent("Animator");
    }

    void FixedUpdate()
    {
        anim.SetBool("Walking", currentState == PlayerController.StateEnum.Walking 
            || currentState == PlayerController.StateEnum.Running);
        anim.SetBool("Running", currentState == PlayerController.StateEnum.Running);
                
        anim.SetBool("Jumping", currentState == PlayerController.StateEnum.Airbourne);
        if (currentState == PlayerController.StateEnum.PunchForward && prevState != PlayerController.StateEnum.PunchForward)
        {
            anim.SetTrigger("Punch");
            
        }
        anim.SetBool("Forward", currentState == PlayerController.StateEnum.PunchForward);
        prevState = currentState;
    }
        
    public void SendState(PlayerController.StateEnum state)
    {
        currentState = state;
    }
}