using UnityEngine;

[AddComponentMenu("Shared/Health Component")]
public class HealthComponent : MonoBehaviour
{
        
    public float health = 100;
    public float maxHealth = 100;
    
    public delegate void OnDeathEvent(GameObject g);

    public event OnDeathEvent OnDeath;

    public delegate void HealthUpdatedEvent(GameObject g,float actualHealth,float percentageHealth,float change);

    public event HealthUpdatedEvent HealthUpdated;

    void Start()
    {
        HealthUpdated += (g, actualHealth, percentageHealth, change) => {
            Debug.Log("Health Updated");};
    }

    // Called by SendMessage from projectiles
    // Must be a positive float number as we are dealing damage
    //  and recognise it's value as a positive amount of damage
    //  rather than a negative loss of health.
    void RegisterDamage(float damageAmount)
    {
        health -= damageAmount;

        HealthUpdated(gameObject, health, CurrentHealthPercentage(), -damageAmount);

        if (health <= 0)
        {
            OnDeath(gameObject);
        }
    }
    
    // Called by SendMessage from healers
    void ApplyHeal(float healAmount)
    {
        if (healAmount >= 0.0f)
        {
            health += healAmount;
            float percentHealth = this.CurrentHealthPercentage();
            //HealthUpdated (gameObject, health, percentHealth, healAmount);
        }
    }

    public float CurrentHealthPercentage()
    {
        return Mathf.Clamp((health / maxHealth) * 100, 0, 100);
    }

}
