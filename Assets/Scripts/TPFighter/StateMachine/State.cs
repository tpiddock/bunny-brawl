using System;

namespace TPFighter
{
    namespace StateMachine
    {
        /** Enumeration describing the type of States. */
        public enum StateType
        {
            ST_INITIALSTATE,        // State is an Initial State
            ST_FINALSTATE,          // State is a Final State
            ST_NORMALSTATE,         // State is a normal State
            ST_COMPOUNDSTATE,       // State is a compound (Hierarchical)
            ST_ABSTRACTSTATE        // State that is supposed to be replaced by one of the above types
        }
        
        /** Enumeration determining the behavior of the State class for unhandled inputs */
        public enum StateBehavior
        {
            SB_STANDARD,        // Go to an indeterminate State when an unhandled input is processed.
            SB_MODIFIED         // Do nothing when an unhandled input is processed. (*default)
        }
        
        public class State
        {    
            
            //Properties and fields
            protected StateType stateType;
            protected string name;
            protected int id;
            protected Transition[] transitionMap;
            protected StateMachine owner;

            public string Name
            {
                get
                {
                    return name;
                }
            }
            
            public int Id
            {
                get
                {
                    return id;
                }
            }
       
            //Constructors
            State(string name, StateBehavior stateBehaviour)
            {
            }

            ~State ()
            {
    
            }

            //Methods
            public void AddStateListener()
            {
            }

            public State ProccessInput(StateMachineInput input, bool changedState)
            {
                State result = new State("Idle", StateBehavior.SB_MODIFIED);
                
                return result;
            }
        }
    }
}

