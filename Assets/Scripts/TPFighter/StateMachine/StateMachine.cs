using System;

namespace TPFighter
{
    namespace StateMachine
    {
        public class StateMachine
        {
            //Properties and fields
            private State initialState;
            private State stateMap;
            private State currentState;
            private string name;

            //Constructors
            public StateMachine(string name)
            {
                this.name = name;
            }

            public State GetCurrentState()
            {
                throw new NotImplementedException();
            }

            public void ProcessInput()
            {
                throw new NotImplementedException();
            }

            public StateMachine GetStateMachineInstance()
            {
                throw new NotImplementedException();
            }

            public void ReleaseStateMachineInstance()
            {
                throw new NotImplementedException();
            }
        }
    }
}
