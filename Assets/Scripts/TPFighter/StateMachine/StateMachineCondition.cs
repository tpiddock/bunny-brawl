using System;

namespace TPFighter
{
    namespace StateMachine
    {
        enum StateMachineConditionType
        {
            SMCT_INTEGEREQUALITY,               /// Maps to IntegerEqualityCondition
            SMCT_STRINGEQUALITY,                /// Maps to StringEqualityCondition
            SMCT_REALEQUALITY,                  /// Maps to RealEqualityCondition
            SMCT_REALRANGE,                     /// Maps to RealRangeCondition
            SMCT_INTEGERRANGE,                  /// Maps to IntegerRangeCondition
            SMCT_ALWAYSTRUE,                    /// Maps to AlwaysTrueCondition
            SMCT_INVALID = 10000                /// Invalid type
        }
        
        public abstract class StateMachineCondition
        {
            public abstract void TestInput();
        }
    }
}