using System;

namespace TPFighter
{
    namespace StateMachine
    {   /** Enumeration describing the possible types of input for StateMachines */
        public enum StateMachineInputType
        {
            SMIT_INTEGER,           /// CAInt input type
            SMIT_REAL,              /// CAReal input type
            SMIT_STRING,            /// string input type
            SMIT_NONE,              /// No input
            SMIT_INVALID = 10000    /// Invalid input type
        }

        /** Class representing an input for a StateMachine. */
        public class StateMachineInput
        {
         
            protected StateMachineInputType type;
            
            public StateMachineInput()
            {
            }
            
            public StateMachineInputType GetStateMachineInputType{ get { return this.type; } }
        }
        
        /** Class representing an Integer input for a StateMachine. */
        public class IntegerStateMachine : StateMachineInput
        {
            protected int val;

            public IntegerStateMachine(int value)
            {
                val = value;
            }
            
            public int GetValue()
            {
                return val;
            }
            
        }
        
        /** A StateMachineInput that is a real number. */
        public class FloatStateMachine : StateMachineInput
        {
            protected float val;

            public FloatStateMachine(float value)
            {
                val = value;
            }
            
            public float GetValue()
            {
                return val;
            }
        }
        
        /** A StateMachineInput that is a string. */
        public class StringStateMachine : StateMachineInput
        {
            protected string val;

            public StringStateMachine(string value)
            {
                val = value;
            }
            
            public string GetValue()
            {
                return val;
            }
        }
    }
}