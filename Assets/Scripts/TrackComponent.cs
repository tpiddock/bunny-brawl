﻿using UnityEngine;
using System.Collections;

public class TrackComponent : MonoBehaviour
{

    public Transform targetTransform;
    private Transform myTransform;
    public Vector3 offset;
    
    // Use this for initialization
    void Start()
    {
        myTransform = transform;
    }
	
    // Update is called once per frame
    void Update()
    {
        myTransform.position = targetTransform.position + offset;
    }
}
