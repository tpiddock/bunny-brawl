using System;


namespace TPInputLibrary
{
    public class InputState
    {
        public struct Axis
        {   
            public float X;
            public float Y;
                        
            public float XNorm
            {
                get{ return Math.Abs(X); }
            }
                        
            public float YNorm
            {
                get{ return Math.Abs(Y); }
            }
        }
            
        public struct ActionStates
        {
            public bool Jump;
            public bool Punch;
            public bool Kick;
            public bool Special;
            public bool Grab;
            public bool Guard;
        }
            
        public Axis MovementAxis;
        public Axis CameraAxis;
        public ActionStates Actions;
                
        public override string ToString()
        {
            string output = "";
                            
            //Actions
            if (this.Actions.Jump)
                output += "Jump executed ";
            if (this.Actions.Punch)
                output += "Punch executed ";
            if (this.Actions.Kick)
                output += "Kick executed ";
            if (this.Actions.Special)
                output += "Special executed ";
            if (this.Actions.Grab)
                output += "Grab executed ";
            if (this.Actions.Guard)
                output += "Guard executed ";
                                
            //Axis
            const double movementTollerance = 0.01;
            if (this.MovementAxis.XNorm > movementTollerance)
                output += "Moving X ";
            if (this.MovementAxis.YNorm > movementTollerance)
                output += "Moving Y ";
            if (this.CameraAxis.XNorm > movementTollerance)
                output += "Moving Camera X ";
            if (this.CameraAxis.YNorm > movementTollerance)
                output += "Moving Camera Y ";
                                
            return output;
        }
    }
}
