using UnityEngine;


namespace TPInputLibrary
{
    public class UnityKeyboardInputConverter : InputConverter
    {
        public override InputState Convert()
        {
            InputState state = new InputState();

            //Actions
            state.Actions.Jump = Input.GetKey(KeyCode.Space);
                
            //Offense
            state.Actions.Punch = Input.GetKey(KeyCode.J);
            state.Actions.Kick = Input.GetKey(KeyCode.K);
            state.Actions.Special = Input.GetKey(KeyCode.L);
                
            //Defense
            state.Actions.Grab = Input.GetKey(KeyCode.RightShift);
            state.Actions.Guard = Input.GetKey(KeyCode.LeftShift);
                
            //Movement
            state.MovementAxis.X = Input.GetAxisRaw("Horizontal");
            state.MovementAxis.Y = Input.GetAxisRaw("Vertical");
                
            //Camera
            state.CameraAxis.X = Input.GetAxisRaw("Mouse X");
            state.CameraAxis.X = Input.GetAxisRaw("Mouse Y");
                
            return state;     
        }        
    }
}

