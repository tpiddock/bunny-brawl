using System; 
using XInputDotNetPure;


namespace TPInputLibrary
{
    public class XInputConverter : InputConverter
    {       
            
        bool playerIndexSet = false;
        PlayerIndex playerIndex;
        GamePadState gamePadState;
        GamePadState prevState;
                
        public XInputConverter(int playerSlot)
        {
            VerifyControllerConnected(playerSlot);
                        
        }
        
        private void VerifyControllerConnected(int playerSlot)
        {
            // Find a PlayerIndex, for a single player game
            if (!playerIndexSet || !prevState.IsConnected)
            {
                PlayerIndex testPlayerIndex = (PlayerIndex)playerSlot - 1;
                GamePadState testState = GamePad.GetState(testPlayerIndex);
                if (testState.IsConnected)
                {
                    //Debug.Log (string.Format ("GamePad found {0}", testPlayerIndex));
                    playerIndex = testPlayerIndex;
                    playerIndexSet = true;
                } else
                {
                    throw new Exception(string.Format("GamePad {0} has been unplugged or is not connecting properly", testPlayerIndex));
                }
            }
        }

        public override InputState Convert()
        {
            InputState outputState = new InputState();
                        
            gamePadState = GamePad.GetState(playerIndex);
                        
            //Actions 
            outputState.Actions.Jump = gamePadState.Buttons.Y == ButtonState.Pressed;
                        
            //Offense
            outputState.Actions.Punch = gamePadState.Buttons.X == ButtonState.Pressed;
            outputState.Actions.Kick = gamePadState.Buttons.A == ButtonState.Pressed;
            outputState.Actions.Special = gamePadState.Buttons.B == ButtonState.Pressed;
                        
            //Defense
            outputState.Actions.Grab = gamePadState.Buttons.LeftShoulder == ButtonState.Pressed || gamePadState.Buttons.RightShoulder == ButtonState.Pressed;
            outputState.Actions.Guard = gamePadState.Triggers.Left > 0 || gamePadState.Triggers.Right > 0;
            
            //Movement from DPad
            short xAxis = 0;
            short yAxis = 0;

            //Determine if Dpad has any input
            if (gamePadState.DPad.Right == ButtonState.Pressed)
                xAxis += 1;
            if (gamePadState.DPad.Left == ButtonState.Pressed)
                xAxis -= 1;
            if (gamePadState.DPad.Up == ButtonState.Pressed)
                yAxis += 1;
            if (gamePadState.DPad.Down == ButtonState.Pressed)
                yAxis -= 1;

            //Set the movement to either the Thumsticks or the DPad 
            outputState.MovementAxis.X = xAxis == 0 ? gamePadState.ThumbSticks.Left.X : xAxis;
            outputState.MovementAxis.Y = yAxis == 0 ? gamePadState.ThumbSticks.Left.Y : yAxis;
                        
            //Camera
            outputState.CameraAxis.X = gamePadState.ThumbSticks.Right.X;
            outputState.CameraAxis.X = gamePadState.ThumbSticks.Right.Y;
                                                
            return outputState;     
        }               
    }  
}


